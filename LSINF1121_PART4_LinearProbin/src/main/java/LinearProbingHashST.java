public class LinearProbingHashST<Key, Value> {
    private static final int INIT_CAPACITY = 4;

    // Please do not add/remove variables/modify their visibility.
    protected int n;           // number of key-value pairs in the symbol table
    protected int m;           // size of linear probing table
    protected Key[] keys;      // the keys
    protected Value[] vals;    // the values


    public LinearProbingHashST() { this(INIT_CAPACITY); }

    public LinearProbingHashST(int capacity) {
        m = capacity;
        n = 0;
        keys = (Key[])   new Object[m];
        vals = (Value[]) new Object[m];
    }

    public int size() { return n; }

    public int capacity() { return m; }

    public boolean isEmpty() { return size() == 0; }

    public boolean contains(Key key) {
        keyIsNull(key, true);
        return get(key) != null;
    }

    private boolean keyIsNull(Key key, boolean raiseError) {
        if (key == null) {
            if (raiseError) { throw new IllegalArgumentException("the key argument is null"); }
            else { return true; }
        } else { return false; }
    }

    // hash function for keys - returns value between 0 and M-1
    protected int hash(Key key) { return (key.hashCode() & 0x7fffffff) % m; }

    /**
    * resizes the hash table to the given capacity by re-hashing all of the keys
    */
    private void resize(int capacity) {
        Key[] nKeys = (Key[])   new Object[capacity];
        Value[] nVals = (Value[]) new Object[capacity];

        for (int i = 0; i < m; i++) {
            if (!keyIsNull(keys[i], false)) {
                int j = (keys[i].hashCode() & 0x7fffffff) % capacity;
                while (!keyIsNull(nKeys[j], false)) {
                    if (nKeys[j].equals(keys[i])) { nVals[j] = vals[i]; return; }
                    j = (j + 1) % capacity;
                }
                nKeys[j] = keys[i]; nVals[j] = vals[i];
            }
        }
        keys = nKeys; vals = nVals; m = capacity;
    }

    /**
     * Inserts the specified key-value pair into the symbol table, overwriting the old
     * value with the new value if the symbol table already contains the specified key.
     * The load factor should never exceed 50% so make sure to resize correctly
     *
     * @param  key the key
     * @param  val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void put(Key key, Value val) {
        keyIsNull(key, true);
        if (n >= m/2) resize(2*m);
        int indexKey = hash(key);
        while (!keyIsNull(keys[indexKey], false)) {
            if (keys[indexKey].equals(key)) { vals[indexKey] = val; return; }
            indexKey = (indexKey+1) % m;
        }
        keys[indexKey] = key; vals[indexKey] = val;
        n++;
    }

    /**
     * Returns the value associated with the specified key.
     * @param key the key
     * @return the value associated with {@code key};
     *         {@code null} if no such value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public Value get(Key key) {
        keyIsNull(key, true);
        int indexKey = hash(key);
        while (!keyIsNull(keys[indexKey], false)) {
            if (keys[indexKey].equals(key)) return vals[indexKey];
            indexKey = (indexKey+1) % m;
        }
        return null;
    }

    /**
    * Returns all keys in this symbol table as an array
    */
    public Object[] keys() {
        Object[] exportKeys = new Object[n];
        int j = 0;
        for (int i = 0; i < m; i++)
            if (!keyIsNull(keys[i], false)) exportKeys[j++] = keys[i];
        return exportKeys;
    }
}