public class TestNodeQueue {
    public static void main(String[] args) throws EmptyQueueException {
        NodeQueue<Integer> nodeQueue = new NodeQueue<>();
        for (int i = 0; i <= 20; i++) nodeQueue.enqueue(i);
        for (int i = 0; i <= 20; i++) {
            // System.out.println(nodeQueue.front());
            System.out.println(nodeQueue.dequeue());
        }
    }
}
