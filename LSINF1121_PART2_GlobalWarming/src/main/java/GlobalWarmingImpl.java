import java.util.*;
import java.util.stream.IntStream;

public class GlobalWarmingImpl extends GlobalWarming {
    int[] altOneLine;

    public GlobalWarmingImpl(int[][] altitude) {
        super(altitude);
        altOneLine = new int[altitude.length* altitude[0].length];
        IntStream.range(0, altitude.length).forEach(i -> System.arraycopy(altitude[i], 0, altOneLine, (i * altitude.length), altitude[0].length));
        Arrays.sort(altOneLine);
    }

    private int binarySearchIndex(int key) {
        int lo = 0; int hi = altOneLine.length-1; int mid;
        while (lo <= hi) {
            mid = (lo + hi)/2;
            if (key < altOneLine[mid]) hi = mid-1;
            else if (altOneLine[mid] < key) lo = mid+1;
            else return mid;
        }
        return lo;
    }

    public int nbSafePoints(int waterLevel) {
        int index = binarySearchIndex(waterLevel);
        while (index < altOneLine.length && altOneLine[index] <= waterLevel) index++;
        return altOneLine.length - index;
    }
}