public class Floor {
    /**
     * @param node La racine d'un arbre 2-3
     * @param x
     * @return La valeur la plus proche possible de x (y compris x lui-meme) contenue dans l'arbre et plus petite que x.
     *         si une telle valeur n'existe pas, null.
     * @throws Exception
     */
    public static Integer floorRec(TwoThreeNode node, int x) throws Exception {
        if (node == null) return null;

        if (x < node.getFirstValue()) return floorRec(node.getLeft(), x);
        else if (x == node.getFirstValue()) return x;

        if (node.isThreeNode()) {
            if (x < node.getSecondValue()) {
                Integer between = floorRec(node.getCenter(), x);
                return between == null ? node.getFirstValue() : between;
            } else if (x > node.getSecondValue()) {
                Integer after = floorRec(node.getRight(), x);
                return after == null ? node.getSecondValue() : after;
            } else return x;
        } else {
            Integer after = floorRec(node.getCenter(), x);
            return after == null ? node.getFirstValue() : after;
        }
    }

    public static Integer floor(TwoThreeNode node, int x) throws Exception {
        TwoThreeNode smallestParentNode = null; TwoThreeNode current = node;
        boolean isLeftValueOn23Node = false;

        while (current != null) {
            if (current.isThreeNode()) { // Is Three Node
                if (x < current.getFirstValue()) { // Go Node Left
                    if (current.getFirstValue() < x) {
                        smallestParentNode = current;
                        isLeftValueOn23Node = false;
                    }
                    current = current.getLeft();


                } else if (current.getSecondValue() < x) { // Go Node Right
                    smallestParentNode = current;
                    isLeftValueOn23Node = false;
                    current = current.getRight();

                } else if (current.getFirstValue() == x || current.getSecondValue() == x) {
                    return x; // GOOD VALUE

                } else { // Go Node Center
                    if (current.getFirstValue() < x) {
                        smallestParentNode = current;
                        isLeftValueOn23Node = true;
                    }
                    current = current.getCenter();


                }
            } else { // Is Two Node
                if (x < current.getFirstValue()) { // Go Node Left
                    if (current.getFirstValue() < x) {
                        smallestParentNode = current;
                    }
                    current = current.getLeft();

                } else if (current.getFirstValue() == x) {
                    return x; // GOOD VALUE

                } else { // Go Node Center
                    if (current.getFirstValue() < x) {
                        smallestParentNode = current;
                    }
                    current = current.getCenter();

                }
            }
        }

        if (smallestParentNode == null) return null;

        if (isLeftValueOn23Node || !smallestParentNode.isThreeNode()) {
            return smallestParentNode.getFirstValue();
        } else {
            return smallestParentNode.getSecondValue();
        }
    }
}
