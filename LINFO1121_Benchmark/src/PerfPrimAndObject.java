import java.util.Arrays;

public class PerfPrimAndObject {
    public static void timeSortPrim() {
        int[] l1 = new int[10000000];
        Arrays.setAll(l1, i -> (int) (Math.random()*1000));
        long startTimeS = System.currentTimeMillis();
        Arrays.sort(l1);
        long endTimeS = System.currentTimeMillis();
        System.out.println("JAVA SORT ALGORITHM WITH ints => " + (endTimeS - startTimeS) + " milliseconds");
    }

    public static void timeSortObjects() {
        Integer[] l2 = new Integer[10000000];
        Arrays.setAll(l2, i -> new Integer((int) (Math.random()*1000)));
        long startTimeS = System.currentTimeMillis();
        Arrays.sort(l2);
        long endTimeS = System.currentTimeMillis();
        System.out.println("JAVA SORT ALGORITHM WITH INTERGERS => " + (endTimeS - startTimeS) + " milliseconds");
    }

    public static void main(String[] args) {
        timeSortPrim();
        timeSortObjects();
    }
}