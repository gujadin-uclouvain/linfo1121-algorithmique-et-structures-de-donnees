import java.util.stream.IntStream;

public class GlobalWarmingImpl extends GlobalWarming {
    private int[] components;
    private int[] weight;
    final int size;
    private int nbOps;

    public GlobalWarmingImpl(int[][] altitude, int waterLevel) { // expected pre-processing time in the constructor : O(n^2 log(n^2)
        super(altitude,waterLevel);
        size = altitude.length;
        nbOps = size*size;
        components = new int[nbOps]; weight = new int[nbOps];
        IntStream.range(0, nbOps).forEachOrdered(i -> { components[i] = i; weight[i] = 1; });

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (isLand(altitude[i][j])) {
                    if (j < size-1 && isLand(altitude[i][j+1])) union(j+i*altitude.length, j+1+i*altitude.length);
                    if (i < size-1 && isLand(altitude[i+1][j])) union(j+i*altitude.length, j+(i+1)*altitude.length);
                } else { nbOps--; }
            }
        }
    }

    private boolean isLand(int coord) {
        return coord > waterLevel;
    }

    private int findRoot(int p) { while (p != components[p]) { p = components[p];} return p; }

    private void union(int p, int q) {
        int i = findRoot(p); int j = findRoot(q);
        if (i == j) return;

        if (weight[i] < weight[j]) { components[i] = j; weight[j] += weight[i]; }
        else { components[j] = i; weight[i] += weight[j]; }
        nbOps--;
    }

    public int nbIslands() { // expected time complexity O(1)
        return nbOps;
    }

    public boolean onSameIsland(Point p1, Point p2) { // expected time complexity O(1) (amortized)
        return !p1.equals(p2) && components[findRoot(p1.y + (p1.x * size))] == components[findRoot(p2.y + (p2.x * size))];
    }

}