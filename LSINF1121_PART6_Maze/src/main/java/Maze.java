import java.util.LinkedList;

public class Maze {
    public static Iterable<Integer> shortestPath(int [][] maze,  int x1, int y1, int x2, int y2) {
        final int xSize = maze.length;
        final int ySize = maze[0].length;
        Index.setNbrColumns(ySize);

        final Index start = new Index(x1, y1);
        final Index end = new Index(x2, y2);
        LinkedList<Index> queue = new LinkedList<>();
        LinkedList<Integer> path = new LinkedList<>();

        if (maze[start.x][start.y] == 1 || maze[end.x][end.y] == 1) return path;
        if (start.x == end.x && start.y == end.y) { path.add(start.index); return path; }

        boolean[][] marked = new boolean[xSize][ySize];
        Index[][] edgeTo = new Index[xSize][ySize];

        marked[start.x][start.y] = true;
        queue.add(start);

        while (!queue.isEmpty()) {
            Index curr = queue.remove();
            for (Index next : edges(maze, curr.x, curr.y)) {
                if (next != null && !marked[next.x][next.y]) {
                    edgeTo[next.x][next.y] = curr;
                    marked[next.x][next.y] = true;
                    queue.add(next);
                }
            }
        }

        if (edgeTo[end.x][end.y] != null) {
            for (Index curr = end; curr != start; curr = edgeTo[curr.x][curr.y]) {
                path.addFirst(curr.index);
            }
            path.addFirst(start.index);
        }

        return path;
    }

    private static Index[] edges(int[][] maze, int x, int y) {
        Index[] tab = new Index[4];
        if (x   > 0              && isFree(maze, x - 1, y)) tab[0] = new Index(x-1, y); // Up
        if (x+1 < maze.length    && isFree(maze, x + 1, y)) tab[1] = new Index(x+1, y); // Down
        if (y   > 0              && isFree(maze, x, y - 1)) tab[2] = new Index(x, y-1); // Left
        if (y+1 < maze[0].length && isFree(maze, x, y + 1)) tab[3] = new Index(x, y+1); // Right
        return tab;
    }

    private static boolean isFree(int[][] map, int x, int y) {
        return map[x][y] == 0;
    }

    public static int ind(int x,int y, int lg) {return x*lg + y;} // Find Index

    public static int row(int pos, int mCols) { return pos / mCols; } // Find X

    public static int col(int pos, int mCols) { return pos % mCols; } // Find Y

    private static class Index {
        int x; int y; int index;
        static int nbrColumns;

        public Index(int x, int y) {
            this.x = x;
            this.y = y;
            this.index = ind(x, y, nbrColumns);
        }

        public static void setNbrColumns(int nbrColumns) {
            Index.nbrColumns = nbrColumns;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Index i = (Index) o;
            return x == i.x && y == i.y;
        }

        @Override
        public String toString() {
            return "(" + x + ", " + y + ")";
        }
    }
}
