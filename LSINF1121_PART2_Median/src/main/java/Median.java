public class Median {

    public static int median(Vector a, int lo, int hi) {
        int mid = a.size() / 2;

        while (lo < hi) {
            int pi = partition(a, lo, hi);
            if (pi == mid) return a.get(mid);
            else if (mid < pi) hi = pi - 1;
            else lo = pi + 1;
        }
        return a.get((hi+lo)/2);
    }

    private static int partition(Vector a, int lo, int hi) {
        int i = lo; int j = hi + 1; int first = a.get(lo);

        while (true) {
            while (a.get(++i) < first) if (i == hi) break;
            while (a.get(--j) > first) if (j == lo) break;
            if (i >= j) break;
            a.swap(i, j);
        }
        a.swap(lo, j);
        return j;
    }
}
