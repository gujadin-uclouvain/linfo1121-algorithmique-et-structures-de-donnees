import java.util.Arrays;

public class Runner {
    public static void main(String[] args) {
        int [][] tab = new int[][] {{1,3,3,1,3},
                                    {4,2,2,4,5},
                                    {4,4,1,4,2},
                                    {1,4,2,3,6},
                                    {1,1,1,6,3}};
        int wl = 3;
        GlobalWarming.Point p1 = new GlobalWarming.Point(1, 0);
        GlobalWarming.Point p2 = new GlobalWarming.Point(3, 1);

        GlobalWarming gw = new GlobalWarmingImpl(tab, wl);
        System.out.println(
                gw.shortestPath(p1, p2)
        );
    }
}
