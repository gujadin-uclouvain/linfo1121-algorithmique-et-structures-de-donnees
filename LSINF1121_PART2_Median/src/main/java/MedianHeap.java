public class MedianHeap {
	
    public static int median(Vector a, int lo, int hi) {
        heapSort(a);
        return a.get(lo+(hi-lo)/2);
    }

    private static void heapify(Vector a, int size, int i) {
        int largest = i; int l = 2*i + 1; int r = 2*i + 2;

        if (l < size && a.get(l) > a.get(largest)) largest = l;
        if (r < size && a.get(r) > a.get(largest)) largest = r;

        if (largest != i) { a.swap(i, largest); heapify(a, size, largest); }
    }

    private static void heapSort(Vector a) {
        int size = a.size();
        for (int i = size/2 - 1; 0 <= i; i--) { heapify(a, size, i); }
        for (int i = size-1; 0 < i; i--) { a.swap(0, i); heapify(a, i, 0); }
    }
}
