import java.util.Arrays;

public class ClosestPair {

    /**
     *
     * @param input a non empty array
     * @return an array containing two values a,b present in the input array (possibly the same value)
     *         such that |x-(a+b)| is minimum
     *
     */
    public static int[] closestPair(int [] input, int x) {
        Arrays.sort(input);
        int lo = 0; int hi = input.length-1;
        int[] pair = new int[]{input[lo], input[hi]};
        int bestDiff = Math.abs(x-(input[lo]+input[hi]));
        while (lo <= hi) {
            int a = input[lo]; int b = input[hi]; int sum = a+b;
            if (sum == x) return new int[]{a, b};

            if (Math.abs(x-sum) < bestDiff) {
                bestDiff = Math.abs(x-sum);
                pair[0] = a; pair[1] = b;
            }

            if (sum > x) hi--;
            else lo++;
        }
        return pair;
    }
}
