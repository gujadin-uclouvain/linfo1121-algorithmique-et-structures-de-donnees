import java.util.Arrays;

public class MinMaxHeap<Key extends Comparable<Key>> {

    // ---------------------------------------------------------------------------------------
    // Instance variables
    // ---------------------------------------------------------------------------------------

    // You are not allowed to add/delete variables, nor modifying their names or visibility.
    public Key[] pq;          // contains the elements starting at position 1
    public int N = 0;         // number of elements in the heap
    public int height = 0;    // should help you to know if you are at a level min or max

    // ---------------------------------------------------------------------------------------------------
    // Functions that you should implement. This is the only part of this class that you should modify ;-)
    // ---------------------------------------------------------------------------------------------------

    /**
     * @pre size() >= 1
     */
    public Key min() { // O(1) expected
        return pq[1];
    }

    /**
     * @pre size() >= 1
     */
    public Key max() { // O(1) expected
        return (size() < 3) ? pq[size()] : (less(2, 3) ? pq[3] : pq[2]);
    }

    /**
     * @pre 1 <= k <= size()
     */
    private void swim(int k) { // O(log N) expected
        boolean kIsInMin = heightIsEven();
        if (kIsInMin) { // MIN LEVEL AT START
            if (lessCheck(k/2, k)) { exch(k, k/2); k = k/2; kIsInMin = false; }
        } else { // MAX LEVEL AT START
            if (lessCheck(k, k/2)) { exch(k, k/2); k = k/2; kIsInMin = true; }
        }

        while (kIsInMin && k > 1 && lessCheck(k, k/4)) { // k is in MIN
            exch(k, k/4);
            k = k/4;
        }
        while (!kIsInMin && k > 1 && lessCheck(k/4, k)) { // k is in MAX
            exch(k, k/4);
            k = k/4;
        }
    }

    /**
     * @pre i, j > 0
     * @return pq[i] < pq[j]
     */
    private boolean lessCheck (int i, int j) { return (i != 0 && j != 0) && less(i, j); }

    /**
     * @return true if height is even; false if odd
     */
    private boolean heightIsEven() { return height % 2 != 0; }


    // ---------------------------------------------------------------------------------------
    // Constructor and helpers. You should not modify this. However, using them may be useful.
    // ---------------------------------------------------------------------------------------

    public MinMaxHeap(int maxN) {
        pq = (Key[]) new Comparable[maxN + 1];
    }

    /**
     * @return pq[i] < pq[j]
     */
    public boolean less(int i, int j) {
        return pq[i].compareTo(pq[j]) < 0;
    }

    /**
     * Exchanges positions i and j in pq
     */
    private void exch(int i, int j) {
        Key e = pq[i];
        pq[i] = pq[j];
        pq[j] = e;
    }

    /**
     * @return true if the heap is empty, false else
     */
    public boolean isEmpty() {
        return N == 0;
    }

    /**
     * @return the size of the heap
     */
    public int size() {
        return N;
    }

    /**
     * @param v value to insert in the heap. v != null.
     */
    public void insert(Key v) {
        pq[++N] = v;
        if (N >= (1 << height)) height++; // Define a new level in the binary tree
        swim(N);
    }

    @Override
    public String toString() {
        return Arrays.toString(pq);
    }
}