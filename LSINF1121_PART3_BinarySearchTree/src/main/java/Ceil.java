
public class Ceil {
	/**
	 * Find in the tree the smallest element greater than or equal to value
	 * (so either the element itself or the element located directly after it
	 * in order of magnitude). If such an element does not exist,
	 * it must return null.
	 */
	public static Integer ceil(Node root, int value) {
		Node biggestParent = null; Node current = root;
		while (true) {
			if (value < current.getValue()) {
				if (current.getLeft() != null) { biggestParent = current; current = current.getLeft(); } // Verify if there is smaller node than current.
				else { return current.getValue(); } // if not, the smaller (current) node in the tree is returned.
			}

			else if (current.getValue() < value) {
				if (current.getRight() != null) { // Verify if there is bigger node than current.
					if (biggestParent != null && biggestParent.getValue() < current.getValue()) { biggestParent = current; } // Verify if the current parent is bigger than the current node.
					current = current.getRight(); // if not, the parent continue to be the smallest element greater than value.
				}
				else { // if not, verify is there is (not) a smallest element greater than value.
					if (biggestParent != null && value < biggestParent.getValue()) { return biggestParent.getValue(); } // Verify is the biggest parent is bigger then value.
					else { return null; } // if not, there is no element greater than value.
				}
			}

			else { return current.getValue(); } // The current value is equal to value.
		}
    }

	public static void main(String[] args) {
		Node root = new Node(12);
		int[] array = new int[]{8, 18, 3, 11, 14, 20, 9, 15};
		for (int i : array) {
			root.add(i);
		}
		for (int i = 0; i < 23; i++) {
			System.out.println("value " + i + " => " + ceil(root, i));
		}
	}
}

