import java.util.Arrays;

public class Runner {
    public static void main(String[] args) {
        Heap heap = new Heap(2);
        heap.push(5);
        System.out.println(Arrays.toString(heap.getContent()));
        heap.push(1);
        System.out.println(Arrays.toString(heap.getContent()));
        heap.push(2);
        System.out.println(Arrays.toString(heap.getContent()));
        heap.push(3);
        System.out.println(Arrays.toString(heap.getContent()));
        heap.push(8);
        System.out.println(Arrays.toString(heap.getContent()));
        heap.push(10);
        System.out.println(Arrays.toString(heap.getContent()));
        heap.push(6);
        System.out.println(Arrays.toString(heap.getContent()));
        heap.push(0);
        System.out.println(Arrays.toString(heap.getContent()));
    }
}
