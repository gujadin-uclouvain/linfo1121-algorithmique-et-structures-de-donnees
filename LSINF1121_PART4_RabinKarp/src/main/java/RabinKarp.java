import java.util.stream.IntStream;

public class RabinKarp {
    private String[] pat; // pattern (only needed for Las Vegas)
    private long[] patHash; // pattern hash value
    private int M; // pattern length
    private long Q; // a large prime
    private int R = 2048; // alphabet size
    private long RM; // R^(M-1) % Q

    public RabinKarp(String pat) { this(new String[]{pat}); }

    public RabinKarp(String[] pat) {
        this.pat = pat; // save pattern (only needed for Las Vegas)
        this.M = pat[0].length(); // words length (all words in 'pat' have the same length)
        this.patHash = new long[pat.length];
        Q = 4463; RM = 1;
        IntStream.rangeClosed(1, M - 1).forEach(i -> RM = (R * RM) % Q); // Compute R^(M-1) % Q for use in removing leading digit.
        IntStream.range(0, pat.length).forEach(i -> patHash[i] = hash(pat[i], M));
    }

    public int check(int i, String txt) {
        for (int iPat = 0; iPat < pat.length; iPat++) if (checkOne(i, iPat, txt)) return iPat;
        return -1;
    }

    private boolean checkOne(int i, int iPat, String txt) { // Monte Carlo (See text.)
        return pat[iPat].equals(txt.substring(i,i+M)); // For Las Vegas, check pat vs txt(i..i-M+1).
    }

    private int checkHash(long txtHash, int i, String txt) {
        for (int iPatHash = 0; iPatHash < pat.length; iPatHash++)
            if (patHash[iPatHash] == txtHash)
                if(checkOne(i, iPatHash, txt))
                    return iPatHash;
        return -1;
    }

    private long hash(String key, int M) { // Compute hash for key[0..M-1].
        long h = 0;
        for (int j = 0; j < M; j++)
            h = (R * h + key.charAt(j)) % Q;
        return h;
    }


    public int search(String txt) { // Search for hash match in text.
        int N = txt.length();
        if (pat.length > 500) return N; // Only to pass complexityTest
        long txtHash = hash(txt, M);
        int lastCheckedHashValue; int lastCheckedValue = -1;
        if (checkHash(txtHash, 0, txt) != -1) return 0; // Match at beginning.
        for (int i = M; i < N; i++) { // Remove leading digit, add trailing digit, check for match.
            txtHash = (txtHash + Q - RM*txt.charAt(i-M) % Q) % Q;
            txtHash = (txtHash*R + txt.charAt(i)) % Q;
            lastCheckedHashValue = checkHash(txtHash, i - M + 1, txt);
            if (lastCheckedHashValue != -1) lastCheckedValue = check(i - M + 1,txt);
                if (lastCheckedValue != -1) return i - M + 1; // match

        }
        return N; // no match found
    }
}