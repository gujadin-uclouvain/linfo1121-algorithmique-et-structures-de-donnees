import java.util.LinkedList;
import java.util.stream.IntStream;

public class DigraphImplem implements Digraph {
    private final int V;
    private int E;
    private LinkedList<Integer>[] adj;

    public DigraphImplem(int V) {
         this.V = V;
         this.E = 0;
         adj = new LinkedList[V];
         IntStream.range(0, V).forEach(v -> adj[v] = new LinkedList<>());
    }

    /**
     * The number of vertices
     */
    public int V() {
        return V;
    }

    /**
     * The number of edges
     */
    public int E() {
        return E;
    }

    /**
     * Add the edge v->w
     */
    public void addEdge(int v, int w) {
        adj[v].add(w); E++;
    }

    /**
     * The nodes adjacent to node v
     * that is the nodes w such that there is an edge v->w
     */
    public Iterable<Integer> adj(int v) {
        return adj[v];
    }

    /**
     * A copy of the digraph with all edges reversed
     */
    public Digraph reverse() {
        Digraph reverseDigraph = new DigraphImplem(V);
        IntStream.range(0, V).forEach(v -> adj(v).forEach(w -> reverseDigraph.addEdge(w, v)));
        return reverseDigraph;
    }


}
