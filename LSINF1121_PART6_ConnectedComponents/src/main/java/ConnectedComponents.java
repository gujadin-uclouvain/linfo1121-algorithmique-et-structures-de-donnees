public class ConnectedComponents {
    /**
     * @return the number of connected components in g
     */
    public static int numberOfConnectedComponents(Graph g) {
        int nbrcc = 0;
        boolean[] marked = new boolean[g.V()];
        for (int v=0; v < g.V(); v++) {
            if (!marked[v]){
                nbrcc += 1;
                marked[v] = true;
            }
            dfs(g, v, marked);
        }
        return nbrcc;
    }

    private static boolean[] dfs(Graph g, int v, boolean[] marked) {
        for (int w : g.adj(v)) {
            if (!marked[w]) {
                marked[w] = true;
                dfs(g, w, marked);
            }
        }
        return marked;
    }
}