public class HashNMore {
    private final int sizeArray;

    public HashNMore(int sizeArray) {
        this.sizeArray = sizeArray;
    }

    public int hash(String s, int lo, int hi) {
        int sumOfChars = 0;

        for (int i = lo; i < hi+1; i++) {
            sumOfChars += Character.getNumericValue(s.charAt(i));
        }
        return sumOfChars % sizeArray;
    }

    public int incrementOne(String s, int lo, int hi, int previousHash) {
        return (previousHash -
                Character.getNumericValue(s.charAt(lo)) +
                Character.getNumericValue(s.charAt(hi+1))) % sizeArray;
    }
    

    public static void main(String[] args) {
        HashNMore hashNMore = new HashNMore(200);
        String s = "Hi there, would you like to sign my petition?";
        int lo = 10; int hi = s.length()-5;
        int hashed = hashNMore.hash(s, lo, hi);
        int nextHashed1 = hashNMore.hash(s, lo+1, hi+1);
        int nextHashed2 = hashNMore.incrementOne(s, lo, hi, hashed);
        System.out.println(nextHashed1 == nextHashed2); /* print true */
    }
}