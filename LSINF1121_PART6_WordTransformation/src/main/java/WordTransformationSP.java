import java.util.*;

public class WordTransformationSP {

    /**
     * Rotate the substring between start and end of a given string s
     * eg. s = HAMBURGER, rotation(s,4,8) = HAMBEGRUR i.e. HAMB + EGRU + R
     * @param s
     * @param start
     * @param end
     * @return rotated string
     */
    public static String rotation(String s, int start, int end) {
        return s.substring(0,start)+new StringBuilder(s.substring(start,end)).reverse().toString()+s.substring(end);
    }

    /**
     * Compute the minimal cost from string "from" to string "to" representing the shortest path
     * @param from
     * @param to
     * @return
     */
    public static int minimalCost(String from, String to) {
        PriorityQueue<Vertex> pq = new PriorityQueue<>();
        HashMap<String, Integer> distTo = new HashMap<>();

        distTo.put(from, 0);
        pq.add(new Vertex(from, 0));

        while (!pq.isEmpty()) {
            Vertex curr = pq.remove();
            for (int i = 0; i < from.length()-1; i++) {
                for (int j = i+2; j <= from.length(); j++) {
                    Vertex next = new Vertex(rotation(curr.getName(), i, j),j-i + curr.getCost());

                    if (distTo.getOrDefault(next.getName(), Integer.MAX_VALUE) > next.getCost()) {
                        if (distTo.containsKey(next.getName())) {
                            distTo.replace(next.getName(), next.getCost()); //pq.remove(next);
                        }
                        else { distTo.put(next.getName(), next.getCost()); }
                        pq.add(next);
                    }
                }
            }
        }
        return distTo.get(to);
    }

    private static class Vertex implements Comparable {
        private String name;
        private int cost;

        public Vertex(String name, int cost) {
            this.name = name;
            this.cost = cost;
        }

        public String getName() { return name; }
        public int getCost() { return cost;}

        @Override
        public int hashCode() {
            return Objects.hashCode(name);
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Vertex)) return false;
            Vertex v = (Vertex) o;
            return name.equals(v.name);
        }

        @Override
        public int compareTo(Object o) {
            return cost - ((Vertex) o).cost;
        }
    }
}