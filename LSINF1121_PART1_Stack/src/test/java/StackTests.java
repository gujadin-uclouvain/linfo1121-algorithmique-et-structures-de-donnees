import org.junit.Test;

import java.util.EmptyStackException;

import static org.junit.Assert.*;

public class StackTests {

    @Test
    public void pushAndPopTest() {
        Stack<Integer> stack = new MyStack<>();
        stack.push(42);
        assertEquals(Integer.valueOf(42), stack.pop());
    }

    @Test
    public void pushAndPopTest2() {
        Stack<Integer> stack = new MyStack<>();
        int seed = (int) (Math.random() * 10) + 5;
        int mult = 1;

        for (int i = 0; i < 7; i++) {
            mult*=2;
            stack.push(seed*mult);
        }
        for (int i = 0; i < 7; i++) {
            assertEquals(Integer.valueOf(seed*mult), stack.pop());
            mult/=2;
        }
    }

    @Test
    public void isEmptyTest() {
        Stack<Integer> stack = new MyStack<>();
        assertTrue(stack.empty());
    }

    @Test
    public void isEmptyTest2() {
        Stack<Integer> stack = new MyStack<>();
        stack.push(32);
        stack.pop();
        assertTrue(stack.empty());
    }

    @Test
    public void isNotEmptyTest() {
        Stack<Integer> stack = new MyStack<>();
        stack.push(32);
        assertFalse(stack.empty());
    }

    @Test
    public void isNotEmptyTest2() {
        Stack<Integer> stack = new MyStack<>();
        stack.push(32);
        stack.peek();
        assertFalse(stack.empty());
    }

    @Test
    public void peekTest() {
        Stack<Integer> stack = new MyStack<>();
        stack.push(22);
        stack.push(44);
        assertEquals(Integer.valueOf(44), stack.peek());
        assertEquals(Integer.valueOf(44), stack.pop());
    }

    @Test
    public void peekExceptionTest() {
        Stack<Integer> stack = new MyStack<>();
        try {
            stack.peek();
            fail();
        } catch (EmptyStackException e) {
            assertTrue(true);
        }
    }

    @Test
    public void popExceptionTest() {
        Stack<Integer> stack = new MyStack<>();
        try {
            stack.pop();
            fail();
        } catch (EmptyStackException e) {
            assertTrue(true);
        }
    }
}

