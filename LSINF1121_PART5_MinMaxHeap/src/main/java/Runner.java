public class Runner {
    public static void main(String[] args) {
        MinMaxHeap<Integer> heap = new MinMaxHeap<>(25);

        heap.insert(8);
        heap.insert(71);
        heap.insert(41);
        heap.insert(31);
        heap.insert(10);
        heap.insert(11);
        heap.insert(16);
        heap.insert(46);
        heap.insert(51);
        heap.insert(31);
        heap.insert(21);
        heap.insert(13);
        heap.insert(12);
        heap.insert(17);
        heap.insert(18);
        heap.insert(35);
        heap.insert(38);
        heap.insert(40);
        System.out.println(heap);
        heap.insert(2);
        System.out.println(heap);
    }
}
