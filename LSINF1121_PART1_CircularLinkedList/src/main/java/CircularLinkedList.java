import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularLinkedList<Item> implements Iterable<Item> {
    private long nOp = 0; // count the number of operations
    private int n;          // size of the stack
    private Node last;   // trailer of the list

    // helper linked list class
    private class Node {
        private Item item;
        private Node next;
    }

    public CircularLinkedList() {
        last = null;
        n = 0;
    }

    public boolean isEmpty() {
        return n == 0;
    }

    public int size() {
        return n;
    }

    private long nOp() {
        return nOp;
    }



    /**
     * Append an item at the end of the list
     * @param item the item to append
     */
    public void enqueue(Item item) {
        if (isEmpty()) { last = new Node(); last.item = item; last.next = last;}
        else {
            Node first = last.next;
            last.next = new Node(); last = last.next;
            last.item = item; last.next = first;
        }
        n++; nOp++;
    }

    /**
     * Removes the element at the specified position in this list.
     * Shifts any subsequent elements to the left (subtracts one from their indices).
     * Returns the element that was removed from the list.
     */
    public Item remove(int index) {
        if (index < 0 || index > size()-1) { throw new IndexOutOfBoundsException(); }
        Node current = last;
        for (int i = 0; i < index%n; i++) { current = current.next; }
        Item item = current.next.item;
        if (size() == 1) { last = null; }
        else {current.next = current.next.next; }
        n--; nOp++;
        return item;
    }


    /**
     * Returns an iterator that iterates through the items in FIFO order.
     * @return an iterator that iterates through the items in FIFO order.
     */
    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ListIterator implements Iterator<Item> {
        private final long fnOp = nOp();
        private int cSize = 0;
        private Node current = last == null ? null : last.next;

        private boolean failFastCheck() {
            if (nOp() == fnOp) { return true; }
            else { throw new ConcurrentModificationException(); }
        }

        @Override
        public boolean hasNext() {
            return failFastCheck() && cSize < size();
        }

        @Override
        public Item next() {
            if (hasNext()) { Item item = current.item; current = current.next; cSize++; return item; }
            else { throw new NoSuchElementException(); }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

    }

}