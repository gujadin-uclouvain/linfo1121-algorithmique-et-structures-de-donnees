import edu.princeton.cs.algs4.*;

import java.awt.*;
import java.util.Arrays;

public class SortCompareLim
{
    public static double time(String alg, Comparable[] a)
    {
        Stopwatch timer = new Stopwatch();
        if (alg.equals("Insertion")) Insertion.sort(a);
        if (alg.equals("Selection")) Selection.sort(a);
        if (alg.equals("Shell")) Shell.sort(a);
        if (alg.equals("Merge")) MergeSort.sort(a);
        if (alg.equals("Quick")) QuickSort.sort(a);
        if (alg.equals("ArraySort")) Arrays.sort(a);
        return timer.elapsedTime();
    }
    public static double timeRandomInput(String alg, Class cls,int N, int T)
    { // Use alg to sort T random arrays of length N.
        double total = 0.0;
        Comparable[] a = new Comparable[N];

        for (int t = 0; t < T; t++)
        { // Perform one experiment (generate and sort an array).
            for (int i = 0; i < N; i++){
                if (cls  == Double.class)
                    a[i] = StdRandom.uniform(-1000,1000);
                if (cls  == Integer.class)
                    a[i] = StdRandom.uniform(N);
                if (cls  == Character.class)
                    a[i] = (char) (StdRandom.uniform(26)+ 'a');
            }
            total += time(alg, a);
        }
        return total;
    }

    public static void drawStandard(int[] n, double[] time, Color color, double maxN, double maxT, String algName) {
        StdDraw.setXscale(-0.5, 1.2 * maxN / 1000.0);
        StdDraw.setYscale(-1.5, maxT * 1.2);

        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.001);
        StdDraw.line(0, 0, 1.1 * maxN / 1000.0, 0);
        StdDraw.line(0, 0, 0, 1.1 * maxN);
        for (int i = 1; i < 1.1 * maxN / 1000.0; i++)
            StdDraw.text(i, -1, i + " K");
        for (double t = 2; t < maxT * 1.1; t += 2)
            StdDraw.text(-0.2, t, t + " s");

        StdDraw.setPenColor(color);
        StdDraw.setPenRadius(0.004);
        for (int i = 1; i < n.length; i++){
            StdDraw.line(n[i-1] / 1000.0, time[i-1],n[i] / 1000.0, time[i]);
            if (i == n.length -1)
                StdDraw.text(n[i] / 1000.0 * 1.3, time[i], algName);
        }
    }

    public static void main(String[] args)
    {
        Class cls = null;
        String[] algs = new String[]{"Merge","Shell","Quick", "ArraySort","Selection", "Insertion"};
        Color [] colors = new Color[]{StdDraw.BOOK_RED, StdDraw.BLUE, StdDraw.GREEN, StdDraw.BOOK_LIGHT_BLUE, StdDraw.YELLOW, StdDraw.PRINCETON_ORANGE};
        try {
            cls = Class.forName("java.lang."+args[0]);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        int N = Integer.parseInt(args[1]);
        int T = Integer.parseInt(args[2]);

        StdDraw.enableDoubleBuffering();
        StdDraw.setCanvasSize(900,600);



        for (int i = 0; i < algs.length; i++) {
            double [] runningTimes = new double[N];
            int [] doublingTab = new int[N];
            int j=0;
            for (int n = 250; j<N; n += n) {
                double time = timeRandomInput(algs[i], cls,n, T);
                runningTimes[j] = time;
                doublingTab[j] = n;
                j++;
                System.out.printf("For %d random %s , %s is %f seconds\n", n, cls.getSimpleName(),algs[i],time);
            }
            drawStandard(doublingTab, runningTimes, colors[i], doublingTab[doublingTab.length-1], 20, algs[i]);
        }

        StdDraw.show();
    }
}