import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.stream.IntStream;

public class MineClimbing {
    /**
    * Returns the minimum distance between (startX, startY) and (endX, endY), knowing that
    * you can climb from one mine block (a,b) to the other (c,d) with a cost Math.abs(map[a][b] - map[c][d])
    *
    * Do not forget that the world is round: the position (map.length,i) is the same as (0, i), etc.
    *
    * map is a matrix of size n times m, with n,m > 0.
    *
    * 0 <= startX, endX < n
    * 0 <= startY, endY < m
    */
    public static int best_distance(int[][] map, int startX, int startY, int endX, int endY) {
        final int xSize = map.length; final int ySize = map[0].length;
        int[][] distTo = new int[xSize][ySize]; // For Dijkstra, disTo is marked with weights !!!
        // No need to find a path, so no need to add edgeTo
        PriorityQueue<Column> pq = new PriorityQueue<>();

        IntStream.range(0, xSize).forEach(i -> Arrays.fill(distTo[i], Integer.MAX_VALUE));

        distTo[startX][startY] = 0;
        pq.add(new Column(startX, startY, 0));

        while (!pq.isEmpty()) {
            Column curr = pq.remove();
            for (Column next : edges(map, curr, xSize, ySize)) {
                int newCost = Math.abs(map[curr.x][curr.y] - map[next.x][next.y]);
                if (distTo[next.x][next.y] > curr.cost() + newCost) {
                    next.setCost(curr.cost() + newCost);
                    distTo[next.x][next.y] = next.cost();
                    pq.add(next);
                }
            }
        }

        return distTo[endX][endY];
    }

    private static int setIndex(int i, int max) {
        if (i >= 0) return i % max;
        return max-1;
    }

    private static Column[] edges(int[][] map, Column c, int xSize, int ySize) {
        Column[] pArray = new Column[4];
        pArray[0] = new Column(setIndex(c.x-1, xSize), c.y, map[setIndex(c.x-1, xSize)][c.y]); // Up
        pArray[1] = new Column(setIndex(c.x+1, xSize), c.y, map[setIndex(c.x+1, xSize)][c.y]); // Down
        pArray[2] = new Column(c.x, setIndex(c.y-1, ySize), map[c.x][setIndex(c.y-1, ySize)]); // Left
        pArray[3] = new Column(c.x, setIndex(c.y+1, ySize), map[c.x][setIndex(c.y+1, ySize)]); // Right
        return pArray;
    }

    private static class Column implements Comparable<Column> {
        private final int x;
        private final int y;
        private int accumulatedCost;

        public Column(int x, int y, int cost) {
            this.x = x;
            this.y = y;
            this.accumulatedCost = cost;
        }

        public int cost() { return accumulatedCost; }
        public void setCost(int cost) { this.accumulatedCost = cost; }

        @Override
        public int compareTo(Column c) {
            return accumulatedCost - c.accumulatedCost;
        }

        @Override
        public boolean equals(Object o) {
            Column c = (Column) o;
            return c.x == x && c.y == y;
        }

        @Override
        public String toString() {
            return "(" + x + ", " + y + ") => " + accumulatedCost;
        }
    }
}