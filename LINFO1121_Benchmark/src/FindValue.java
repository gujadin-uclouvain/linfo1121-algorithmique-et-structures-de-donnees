public class FindValue {
    private final int sizeArray;

    public FindValue(int sizeArray) {
        this.sizeArray = sizeArray;
    }

    public int hash(String s, int lo, int hi) {
        int sumOfChars = 0;

        for (int i = lo; i < hi+1; i++) {
            sumOfChars += Character.getNumericValue(s.charAt(i));
        }
        return sumOfChars % sizeArray;
    }


    public int incrementOne(String s, int lo, int hi, int previousHash) {
        return (previousHash -
                Character.getNumericValue(s.charAt(lo)) +
                Character.getNumericValue(s.charAt(hi+1))) % sizeArray;
    }

    /**
     * @param s     The long string (M size)
     * @param subS  The sub string (N size)
     * @return      return the index of where the sub string begins in the long string
     *              If it not exist, return -42
     */
    public int findIndex(String s, String subS) {
        int N = s.length();
        int M = subS.length();
        int hashToFind = hash(subS, 0, M-1);
        int currentHash = hash(s, 0, M-1);
        for (int i = 0; i <= N-M; i++) {
            if (currentHash == hashToFind) {
                String sub = s.substring(i, i + M);
                if (sub.equals(subS)) return i;
            }
            currentHash = incrementOne(s, i, i+M-1, currentHash);
        }
        return -42;
    }

    public static void main(String[] args) {
        FindValue hashNMore = new FindValue(200);
        String s = "Hi there, would you like to sign my petition?";
        String subS = "like to";
        System.out.println(hashNMore.findIndex(s, subS));
    }
}
