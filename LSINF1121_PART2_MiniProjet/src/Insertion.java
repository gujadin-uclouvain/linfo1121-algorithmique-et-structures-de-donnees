public class Insertion{

  public static void sort(Comparable[] a){
    int len = a.length;
    for (int i = 1; i < len; i++){
      for (int j = i; (j > 0) && (a[j].compareTo(a[j-1]) < 0); j--) {
        Comparable t = a[j];
        a[j] = a[j-1];
        a[j-1] = t;
      }
    }
  }
}
