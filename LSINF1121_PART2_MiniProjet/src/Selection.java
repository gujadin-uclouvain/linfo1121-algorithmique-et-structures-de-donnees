public class Selection {
    public static void sort(Comparable[] a) {
        int size = a.length;
        for (int i = 0; i < size; i++) {
            int min = i;
            for (int j = i+1; j < size; j++) {
                if (a[j].compareTo(a[min]) < 0) min = j;
            }
            Comparable temp = a[i];
            a[i] = a[min];
            a[min] = temp;
        }
    }
}
