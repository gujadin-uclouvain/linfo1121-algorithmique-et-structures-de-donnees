import java.util.*;

public class GlobalWarmingImpl extends GlobalWarming {

    /**
     * In the following, we assume that the points are connected to
     * horizontal or vertical neighbors but not to the diagonal ones
     * @param altitude is a n x n matrix of int values representing altitudes (positive or negative)
     * @param waterLevel is the water level, every entry <= waterLevel is flooded
     */
    public GlobalWarmingImpl(int[][] altitude, int waterLevel) {
        super(altitude,waterLevel);
    }


    /**
     *
     * @param p1 a safe point with valid coordinates on altitude matrix
     * @param p2 a safe point (different from p1) with valid coordinates on altitude matrix
     * @return the shortest simple path (vertical/horizontal moves) if any between from p1 to p2 using only vertical/horizontal moves on safe points.
     *         an empty list if not path exists (i.e. p1 and p2 are not on the same island).
     */
    public List<Point> shortestPath(Point p1, Point p2) {
        boolean[][] marked = new boolean[altitude.length][altitude[0].length];
        Point[][] edgeTo = new Point[altitude.length][altitude[0].length];
        LinkedList<Point> pointsList = new LinkedList<>();
        if (isNotIsland(p1) || isNotIsland(p2)) return pointsList;
        marked[p1.x][p1.y] = true;
        pointsList.add(p1);
        while (!pointsList.isEmpty()) {
            Point pCurr = pointsList.remove();
//            System.out.println("Current Point:");
//            System.out.println(pCurr);
            for (Point pNext : edges(pCurr)) {
                if (pNext != null && !marked[pNext.x][pNext.y]) {
                    edgeTo[pNext.x][pNext.y] = pCurr;
                    marked[pNext.x][pNext.y] = true;
                    pointsList.add(pNext);
//                    System.out.println("    Next Point:");
//                    System.out.println("    " + pNext);
                }
            }
        }

        if (!marked[p2.x][p2.y]) return pointsList;
        for (Point pCurr = p2; !pCurr.equals(p1); pCurr = edgeTo[pCurr.x][pCurr.y]) {
            pointsList.addFirst(pCurr);
        }
        pointsList.addFirst(p1);
        return pointsList;
    }

    private boolean isAboveWaterLevel(int x, int y) { return altitude[x][y] > waterLevel; }

    private boolean isNotIsland(Point p) { return !isAboveWaterLevel(p.x, p.y); }

    private Point[] edges(Point p) {
        Point[] pArray = new Point[4];
        if (p.x-1 >= 0                 && isAboveWaterLevel(p.x-1, p.y)) pArray[0] = new Point(p.x-1, p.y); // Up
        if (p.x+1 < altitude.length    && isAboveWaterLevel(p.x+1, p.y)) pArray[1] = new Point(p.x+1, p.y); // Down
        if (p.y-1 >= 0                 && isAboveWaterLevel(p.x, p.y-1)) pArray[2] = new Point(p.x, p.y-1); // Left
        if (p.y+1 < altitude[0].length && isAboveWaterLevel(p.x, p.y+1)) pArray[3] = new Point(p.x, p.y+1); // Right
        return pArray;
    }

}