import java.util.Iterator;

public class TestIteratorBST {
    public static void main(String[] args) {
        BST<Integer, Integer> bst = new BST<>();
        int[] array = new int[]{12, 8, 18, 3, 11, 14, 20, 9, 15};
        for (int i : array) {
            bst.put(i, i);
        }

        Iterator<Integer> iterator = bst.iterator();
        for (int i = 0; i < bst.size(); i++) {
            System.out.println(iterator.next());
        }
    }
}
