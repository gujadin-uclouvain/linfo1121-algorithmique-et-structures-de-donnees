import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class RedBlackTests {
    Random random = new Random();

    @Test
    public void putGetSimpleTest() {
        MyRedBlack<Integer, Integer> studentRB = new MyRedBlack<>();
        for (int i = 0; i < 10; i++) studentRB.put(i, i);
        for (Integer i = 0; i < 10; i++) Assert.assertEquals(i, studentRB.get(i));
    }

    @Test
    public void putGetRandomTest() {
        MyRedBlack<Double, Double> studentRB = new MyRedBlack<>();
        Double[] array = new Double[100];
        Double currentRand;
        for (int i = 0; i < array.length; i++) {
            currentRand = random.nextDouble()*100;
            array[i] = currentRand;
            studentRB.put(currentRand, currentRand);
        }
        for (Double elem : array) Assert.assertEquals(elem, studentRB.get(elem));
    }

    @Test
    public void isEmptyTest() {
        MyRedBlack<Integer, Integer> studentRB = new MyRedBlack<>();
        Assert.assertTrue(studentRB.isEmpty());
        studentRB.put(0, 0);
        Assert.assertFalse(studentRB.isEmpty());
    }

    @Test
    public void sizeRandomTest() {
        MyRedBlack<Integer, Integer> studentRB = new MyRedBlack<>();
        int randMin = 20; int randMax = 100;
        int randSize = (int) (randMin + (random.nextDouble() * (randMax+randMin)));
        for (int i = 0; i < randSize; i++) studentRB.put(i, i);
        Assert.assertEquals(randSize, studentRB.size());
    }

    @Test
    public void containsRandomTest() {
        MyRedBlack<Double, Double> studentRB = new MyRedBlack<>();
        Double[] array = new Double[10];
        Double currentRand;
        for (int i = 0; i < array.length; i++) {
            currentRand = random.nextDouble()*100;
            array[i] = currentRand;
            studentRB.put(currentRand, currentRand);
        }
        Assert.assertTrue(studentRB.contains(array[5]));
        Assert.assertFalse(studentRB.contains(1042.0));
    }

    @Test
    public void deleteMinRandomTest() {
        MyRedBlack<Integer, Integer> studentRB = new MyRedBlack<>();
        int currentRand = (int) (random.nextDouble()*100);
        for (int i = 1; i <= 20; i++) studentRB.put(currentRand * i, currentRand * i);
        studentRB.deleteMin();
        Assert.assertFalse(studentRB.contains(currentRand));
    }

    @Test
    public void deleteMaxRandomTest() {
        MyRedBlack<Integer, Integer> studentRB = new MyRedBlack<>();
        int currentRand = (int) (random.nextDouble()*100);
        for (int i = 1; i <= 20; i++) studentRB.put(currentRand * i, currentRand * i);
        studentRB.deleteMax();
        Assert.assertFalse(studentRB.contains(currentRand * 20));
    }

    @Test
    public void heightTest() {
        MyRedBlack<Character, Character> studentRB = new MyRedBlack<>();
        Character[] characters = new Character[]{'C', 'A', 'H', 'E', 'P', 'S', 'X', 'R', 'L', 'J', 'M'};
        for (Character character: characters) studentRB.put(character, character);
        Assert.assertEquals(3, studentRB.height());
    }
}