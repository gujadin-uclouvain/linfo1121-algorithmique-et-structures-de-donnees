public class NodeQueue<E> {
    // Variables d'instance
    private Node<E> marker;
    private int size;

    // Constructeur
    public NodeQueue() {}

    public int size() { return size; }
    public boolean isEmpty() { return (size==0); }

    public E front() throws EmptyQueueException {
        // COMPLEXITY EXPECTED: O(1)
        if (!isEmpty()) return marker.getNext().getElement();
        throw new EmptyQueueException("The Queue is empty");
    }

    public void enqueue(E element) {
        // COMPLEXITY EXPECTED: O(1)
        Node<E> newNode = new Node<>(element, null);
        if (isEmpty()) { marker = newNode; marker.setNext(marker); }
        else {
            Node<E> firstNode = marker.getNext();
            marker.setNext(newNode);
            marker = marker.getNext();
            marker.setNext(firstNode);
        }
        size+=1;
    }

    public E dequeue() throws EmptyQueueException {
        // COMPLEXITY EXPECTED: O(1)
        if (!isEmpty()) {
            E elem = marker.getNext().getElement();
            marker.setNext(marker.getNext().getNext());
            size-=1;
            return elem;
        }
        throw new EmptyQueueException("The Queue is empty");
    }
}

