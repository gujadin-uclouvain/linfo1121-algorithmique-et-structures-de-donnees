public class Runner {
    public static Graph genGraph() {
        Graph g = new Graph(11);
        g.addEdge(0, 6);
        g.addEdge(0, 7);
        g.addEdge(6, 7);

        g.addEdge(2, 9);
        g.addEdge(2, 4);
        g.addEdge(9, 10);
        g.addEdge(4, 10);
        g.addEdge(4, 5);
        g.addEdge(5, 3);
        g.addEdge(5, 8);
        return g;
    }

    public static void main(String[] args) {
        Graph g = genGraph();
        System.out.println(ConnectedComponents.numberOfConnectedComponents(g));
    }
}
