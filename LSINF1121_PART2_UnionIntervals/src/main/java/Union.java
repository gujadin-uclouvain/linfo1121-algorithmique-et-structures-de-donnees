import java.util.ArrayList;
import java.util.Arrays;

public class Union {

    public static Interval [] union(Interval [] intervals) {
        if (intervals.length <= 1) { return intervals; }
        Arrays.sort(intervals);
        ArrayList<Interval> result = new ArrayList<>(intervals.length);
        int cmin = intervals[0].min; int cmax = intervals[0].max;

        for (int i = 1; i < intervals.length; i++) {
            if (intervals[i].min < cmin) { cmin = intervals[i].min; }

            if (intervals[i].min <= cmax && cmax < intervals[i].max) { cmax = intervals[i].max; }
            else if (cmax < intervals[i].min) {
                result.add(new Interval(cmin, cmax));
                cmin = intervals[i].min;
                cmax = intervals[i].max;
            }
        }
        
        result.add(new Interval(cmin, cmax));
        return result.toArray(new Interval[0]);
    }
}
